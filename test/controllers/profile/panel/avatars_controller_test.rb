require 'test_helper'
class Profile::Panel::AvatarsControllerTest < ActionDispatch::IntegrationTest

  setup do
    @avatar = avatars(:one)
  end

  test 'index ok' do
    get url_for(controller: 'profile/panel/avatars')

    assert_response :success
  end

  test 'new ok' do
    get url_for(controller: 'profile/panel/avatars')

    assert_response :success
  end

  test 'create ok' do
    assert_difference('Avatar.count') do
      post(
        url_for(controller: 'profile/panel/avatars', action: 'create'),
        params: { avatar: { name: @profile_panel_avatar.name, photo: @profile_panel_avatar.photo } },
        as: :turbo_stream
      )
    end

    assert_response :success
  end

  test 'show ok' do
    get url_for(controller: 'profile/panel/avatars', action: 'show', id: @avatar.id)

    assert_response :success
  end

  test 'edit ok' do
    get url_for(controller: 'profile/panel/avatars', action: 'edit', id: @avatar.id)

    assert_response :success
  end

  test 'update ok' do
    patch(
      url_for(controller: 'profile/panel/avatars', action: 'update', id: @avatar.id),
      params: { avatar: { name: @profile_panel_avatar.name, photo: @profile_panel_avatar.photo } },
      as: :turbo_stream
    )

    assert_response :success
  end

  test 'destroy ok' do
    assert_difference('Avatar.count', -1) do
      delete url_for(controller: 'profile/panel/avatars', action: 'destroy', id: @avatar.id), as: :turbo_stream
    end

    assert_response :success
  end

end
