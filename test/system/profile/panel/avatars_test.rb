require "application_system_test_case"

class AvatarsTest < ApplicationSystemTestCase
  setup do
    @profile_panel_avatar = profile_panel_avatars(:one)
  end

  test "visiting the index" do
    visit profile_panel_avatars_url
    assert_selector "h1", text: "Avatars"
  end

  test "should create avatar" do
    visit profile_panel_avatars_url
    click_on "New avatar"

    fill_in "Name", with: @profile_panel_avatar.name
    fill_in "Photo", with: @profile_panel_avatar.photo
    click_on "Create Avatar"

    assert_text "Avatar was successfully created"
    click_on "Back"
  end

  test "should update Avatar" do
    visit profile_panel_avatar_url(@profile_panel_avatar)
    click_on "Edit this avatar", match: :first

    fill_in "Name", with: @profile_panel_avatar.name
    fill_in "Photo", with: @profile_panel_avatar.photo
    click_on "Update Avatar"

    assert_text "Avatar was successfully updated"
    click_on "Back"
  end

  test "should destroy Avatar" do
    visit profile_panel_avatar_url(@profile_panel_avatar)
    click_on "Destroy this avatar", match: :first

    assert_text "Avatar was successfully destroyed"
  end
end
