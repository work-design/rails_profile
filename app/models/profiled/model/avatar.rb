module Profiled
  module Model::Avatar
    extend ActiveSupport::Concern

    included do
      attribute :name

      has_one_attached :photo
    end

  end
end
